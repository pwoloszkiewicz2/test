#include <iostream>

struct xxx
{
    int a = 10;
    int c = 30;
    int b = 20;
};

xxx fun();

int main() 
{
    xxx s = fun(); 
    std::cout << s.a << ' ' << s.b << ' ' << s.c << '\n';
    return 0;
}
