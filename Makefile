normal:
	g++ -o run -std=c++11 main.cc fun.cc 

warn:

	g++ -o run -flto -Wodr -std=c++11 main.cc fun.cc 
    

clean:
	rm *.o run

